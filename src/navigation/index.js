import React,{useState, useEffect} from  'react';
import SignIn from '../screens/SignIn'
import Home from '../screens/Home/Home';
import Details from '../screens/Home/Details';
import { Button, Text, View, ActivityIndicator } from 'react-native';
import { NavigationContainer } from '@react-navigation/native';
//import { createNativeStackNavigator } from '@react-navigation/native-stack';
import { createBottomTabNavigator } from '@react-navigation/bottom-tabs';
import Icon from 'react-native-vector-icons/MaterialIcons';
import list from '../screens/list';
import SignUp from '../screens/SignUp';
//import Details from '../screens/Home/Details';

const Navigation =()=>{

    // const [isLoading, setIsLoading] = React.useState(true);
    // const[userToken, setUserToken] = React.useState(null);

    // useEffect(()=> {
    //     setTimeout(()=> {
    //         setIsLoading(false);
    //     }, 1000);
    // }, []);

    // if(isLoading){
    //     return(
    //         <View style={{flex:1, justifyContent: 'center', alignItems: 'center'}}>
    //             <ActivityIndicator size="large"/>
    //         </View>
    //     )
    // }


    const Tab = createBottomTabNavigator();
    return(
        <NavigationContainer>
            <Tab.Navigator tabBarOptions={{activeTintColor: "red", inactiveTintColor: 'black'}}>
                <Tab.Screen name="Login" component={SignIn} options={{tabBarIcon: ({color}) => (<Icon name="person" color={color} size={30}/>)}} />
                <Tab.Screen name="SignUp" component={SignUp} options={{tabBarIcon: ({color}) => (<Icon name="add" color={color} size={30}/>)}} />
                <Tab.Screen name="Home" component={Home} options={{tabBarIcon: ({color}) => (<Icon name="home" color={color} size={30}/>)}} />
                <Tab.Screen name="Details" component={Details} options={{tabBarIcon: ({color}) => (<Icon name="info" color={color} size={30}/>)}} />
                <Tab.Screen name="list" component={list} options={{tabBarIcon: ({color}) => (<Icon name="list" color={color} size={30}/>)}} />
            </Tab.Navigator>
        </NavigationContainer>
 
    )
    
}

export default Navigation;








