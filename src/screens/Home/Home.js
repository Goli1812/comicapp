import React from 'react';
import {
  View,
  SafeAreaView,
  Text,
  Image,
  StyleSheet,
  FlatList,
  width,
  height,
  TextInput,
   TouchableOpacity,
   ScrollView,
   Dimensions,
} from 'react-native';
import Icon from 'react-native-vector-icons/MaterialIcons';
import COLORS from './colors';
import coms from './coms';
//import {useNavigation } from '@react-navigation/native';

const comCategories = [
  {name: 'DRAMA', icon: "favorite"},
  {name: 'TIME-TRAVEL', icon: "watch"},
  {name: 'FANTASY', icon: "star"},
  {name: 'ROMCOM', icon: "mood"},
  {name: 'ACTION' , icon: "local-fire-department"},
];



const Card = ({com, navigation}) => {
  //const navigation = useNavigation();
  return (
    <TouchableOpacity
      onPress={() => navigation.navigate('Details', com)}>
      <View style={style.cardContainer}>
      <View style={style.cardImageContainer}>
          <Image
            source= {com.img}
            style={{
              width: '100%',
              height: '100%',
              resizeMode: 'contain',
            }}
          />
        </View>


       
        <View style={style.cardDetailsContainer}>
         
          <View style={{flexDirection: 'row', justifyContent: 'space-between'}}>
            <Text
              style={{fontWeight: 'bold', color: "red", fontSize: 18}}>
              {com?.name}
            </Text>
          </View>

         
          <Text
              style={{fontWeight: 'bold', color: COLORS.dark, fontSize: 18}}>
          Rating: {com?.Rating}
          </Text>
        </View>
      </View>
    </TouchableOpacity>
  );
};

const Home = ({navigation}) => {
  const [selectedCategoryIndex, setSeletedCategoryIndex] = React.useState(0);
  const [filteredComs, setFilteredComs] = React.useState([]);

  const fliterCom = index => {
    const currentComs = coms.filter(
      item => item?.com?.toUpperCase() == comCategories[index].name,
    )[0]?.coms;
    setFilteredComs(currentComs);
  };

  React.useEffect(() => {
    fliterCom(0);
  }, []);

  return (
    
    <SafeAreaView style={{flex: 1, color: COLORS.white}}>
       <View style={style.header}>
       {/* <Icon name="arrow-back" size={28} onPress={() => navigation.goBack()} />
     */}
      <View>
      <Text style={{fontSize: 32, fontWeight: 'bold',color: COLORS.dark}}>
        Welcome To
      </Text>
      <Text style={{fontSize: 32, fontWeight: 'bold',color: COLORS.red}}>
        WEB COMICS
      </Text>
     
       

    </View>
    <Image
          source={require('../../assets/logo.jpeg')}
          style={{height: 60, width: 60, borderRadius: 35}}
        />
    </View>
      <ScrollView showsVerticalScrollIndicator={false}>
        <View style={style.mainContainer}>
        
          <View style={style.searchInputContainer}>
            <Icon name="search" size={24} color={COLORS.grey} />
            <TextInput
              placeholderTextColor={COLORS.grey}
              placeholder="Search comics to read"
              style={{flex: 1}}
            />
          </View>

          {/* Render all the categories */}
          <View
            style={{
              flexDirection: 'row',
              justifyContent: 'space-between',
              marginTop: 45,
            }}>
            {comCategories.map((item, index) => (
              <View key={'com' + index} style={{alignItems: 'center'}}>
                <TouchableOpacity
                  onPress={() => {
                    setSeletedCategoryIndex(index);
                    fliterCom(index);
                  }}
                  style={[
                    style.categoryBtn,
                    {
                      backgroundColor:
                        selectedCategoryIndex == index
                          ? COLORS.primary
                          : COLORS.white,
                    },
                  ]}>
                  <Icon
                    name={item.icon}
                    size={30}
                    color={
                      selectedCategoryIndex == index
                        ? COLORS.red
                        : COLORS.primary
                    }
                  />
                </TouchableOpacity>
                <Text style={style.categoryBtnName}>{item.name}</Text>
              </View>
            ))}
          </View>

          {/* Render the cards with flatlist */}
          
          <View style={{marginTop: 20}}>
            <FlatList
              showsVerticalScrollIndicator={false}
              data={filteredComs}
              renderItem={({item}) => (
                <Card com={item} navigation={navigation} />
              )}
            />
          </View>
        </View>
      </ScrollView>
    </SafeAreaView>
  );
};

const style = StyleSheet.create({
  header: {
    padding: 20,
    flexDirection: 'row',
    justifyContent: 'space-between',
    alignItems: 'center',
  },
  mainContainer: {
    flex: 1,
    backgroundColor: COLORS.light,
    borderTopLeftRadius: 20,
    borderTopRightRadius: 20,
    marginTop: 20,
    paddingHorizontal: 20,
    paddingVertical: 40,
    minHeight: height,
  },
  searchInputContainer: {
    height: 50,
    backgroundColor: COLORS.white,
    borderRadius: 7,
    paddingHorizontal: 20,
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'space-between',
  },
  categoryBtn: {
    height: 50,
    width: 50,
    alignItems: 'center',
    justifyContent: 'center',
    borderRadius: 10,
  },
  categoryBtnName: {
    color: COLORS.dark,
    fontSize: 10,
    marginTop: 5,
    fontWeight: 'bold',
  },
  cardContainer: {
    flexDirection: 'row',
    alignItems: 'center',
    marginBottom: 30,
  },
  cardDetailsContainer: {
    height: 150,
    backgroundColor: "white",
    flex: 1,
    borderTopRightRadius: 10,
    borderBottomRightRadius: 10,
    padding: 20,
    justifyContent: 'center',
  },
  cardImageContainer: {
    height: 150,
    width: 140,
    backgroundColor:"white",
    borderRadius: 20,
  },
  imageContainer: {
    flex: 0.5,
    marginTop: 20,
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: 'pink',
  },
});
export default Home;
